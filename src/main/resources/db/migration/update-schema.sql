

ALTER TABLE users
    alter CONSTRAINT uc_user_password UNIQUE (password);

ALTER TABLE forgot_password_tokens
    ADD CONSTRAINT FK_FORGOT_PASSWORD_TOKENS_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE quiz
    ADD CONSTRAINT FK_QUIZ_ON_CREATEDBY FOREIGN KEY (created_by_id) REFERENCES users (id);

ALTER TABLE registration_tokens
    ADD CONSTRAINT FK_REGISTRATION_TOKENS_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);


ALTER TABLE users
    ADD CONSTRAINT uc_user_password UNIQUE (password);

ALTER TABLE forgot_password_tokens
    ADD CONSTRAINT FK_FORGOT_PASSWORD_TOKENS_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE quiz
    ADD CONSTRAINT FK_QUIZ_ON_CREATEDBY FOREIGN KEY (created_by_id) REFERENCES users (id);

ALTER TABLE registration_tokens
    ADD CONSTRAINT FK_REGISTRATION_TOKENS_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);


ALTER TABLE users
    ADD CONSTRAINT uc_user_password UNIQUE (password);

ALTER TABLE forgot_password_tokens
    ADD CONSTRAINT FK_FORGOT_PASSWORD_TOKENS_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE quiz
    ADD CONSTRAINT FK_QUIZ_ON_CREATEDBY FOREIGN KEY (created_by_id) REFERENCES users (id);

ALTER TABLE registration_tokens
    ADD CONSTRAINT FK_REGISTRATION_TOKENS_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

alter table forgot_password_tokens
    add constraint FKfsp9ir1e3nmonoa5tmtsaxssx foreign key (user_id) REFERENCES users (id);
alter table quiz add constraint FKs8lo0w5qbupnrp6y8i6hrdqb8 foreign key (created_by_id) REFERENCES users (id);
alter table registration_tokens add constraint FKqtg6jo3rumvt24kubarl463v8 foreign key (user_id) REFERENCES users (id);
